package id.ac.ub.papb.async2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;

import org.xml.sax.SAXException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

public class MainActivity extends AppCompatActivity {
    final static String source = "https://medium.com/feed/tag/programming";
    RecyclerView rv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rv1 = findViewById(R.id.rv1);
        AsyncExample task=new AsyncExample(rv1);
        rv1.setLayoutManager(new LinearLayoutManager(MainActivity.this));
        task.execute();
    }

    class AsyncExample extends AsyncTask<Void,Void, ArrayList<RssItem>> {
        RecyclerView rv1;
        ProgressDialog progressDialog = new ProgressDialog(MainActivity.this);

        public AsyncExample(RecyclerView rv1) {
            this.rv1 = rv1;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please Wait....");
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(ArrayList<RssItem> arrayLists) {
            super.onPostExecute(arrayLists);
            RssAdapter adapter = new RssAdapter(MainActivity.this,arrayLists);
            rv1.setAdapter(adapter);
            adapter.notifyDataSetChanged();
            progressDialog.dismiss();
        }

        @Override
        protected ArrayList<RssItem> doInBackground(Void... voids) {
            RssParser parser = new RssParser();
            try {
                String xml = parser.loadRssFromUrl(source);
                ArrayList<RssItem>arrayLists =parser.parseRssFromUrl(xml);
                return arrayLists;
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            } catch (SAXException e) {
                e.printStackTrace();
            }
            return null;
        }
    }


}